%% Compare design ratios performance for real data for different techniques to select ratio

%% Setup of the run
dispErrors = 0;

iterationNumber = 20;

% set covariance function for training 
options = {};
options.debugInfo = 0;
covarianceOptions.power = 1;
% options.covarianceFunction = {@covarianceExpArd, covarianceOptions};
options.covarianceFunction = {@covarianceMatern32, covarianceOptions};
dfOptions = {};
dfOptions.debugInfo = 0;
dfOptions.fine = options;
dfOptions.coarse = options;    
% 
% realDataNameList = {'airfoil', 'disk', 'euler', ...
%   'machAngle', 'press12', 'press13', 'supernova', 'svm'};

realDataNameList = {'airfoil', 'disk', 'euler', ...
  'machAngle', 'press12', 'press13'};


realDataNameList = {'svm'};

for realDataNameCell = realDataNameList
  realDataName = realDataNameCell{1};
  disp(realDataName);
  outputDimension = 1;
  costRatioList = [5];
  costRatioNumber = length(costRatioList);
  for costRatio=costRatioList

    % get setup parameter
    budget = 300;
    errorsArray = zeros(iterationNumber, 5);

    for iteration = 1:iterationNumber
      disp(['Iteration:', int2str(iteration)]);

      % generate sample
      [points, lowFidelityValues, highFidelityValues, ...
       extraLowFidelityPoints, extraLowFidelityValues, ...
       inputDimension, ~] = loadRealData(realDataName, outputDimension); 

  %     correlationEstimateSampleSize = 20;
  %     correlationEstimate = corr(highFidelityTrainingValues(1:correlationEstimateSampleSize), ...
  %                                lowFidelityValues(1:correlationEstimateSampleSize));
      correlationEstimate = corr(highFidelityValues, ...
                                 lowFidelityValues);

      rhoEstimate = 1 / (1 / correlationEstimate^2 - 1)^0.5;
      % Run procedure
      optimalDesignSizeRatio = (costRatio * rhoEstimate^2)^(inputDimension / (inputDimension + 2)); % from theorem
      disp(['Optimal ratio:', num2str(optimalDesignSizeRatio)]);

      sampleSizeArray = [[budget / costRatio, 0]; ...
                         [budget / (costRatio + 1), budget / (costRatio + 1)]; ...
                         [budget / 2 / costRatio, budget / 2]; ...
                         [budget / (costRatio + optimalDesignSizeRatio), ...
                          optimalDesignSizeRatio * budget / (costRatio + optimalDesignSizeRatio)]; ...
                         [0, budget]];
      sampleSizeArray(:, 1) = round(sampleSizeArray(:, 1));
      sampleSizeArray(sampleSizeArray(:, 1) < inputDimension + 1, 1) = inputDimension + 1;
      sampleSizeArray(end, 1) = 0;
      sampleSizeArray(:, 2) = budget - costRatio * sampleSizeArray(:, 1);  

      maxHighFidelitySize = max(sampleSizeArray(:, 1));

      % permute sample
      permutation = randperm(size(points, 1));
      points = points(permutation, :);
      lowFidelityValues = lowFidelityValues(permutation, :);
      highFidelityValues = highFidelityValues(permutation, :);

      % split sample
      if size(extraLowFidelityPoints, 1) > budget
        upperIndex = maxHighFidelitySize;
      else
        upperIndex = budget;
      end
      lowFidelityPoints = [points(1:upperIndex, :); ...
                           extraLowFidelityPoints];
      lowFidelityValues = [lowFidelityValues(1:upperIndex); ...
                           extraLowFidelityValues];
      highFidelityTrainingPoints = points(1:maxHighFidelitySize, :);
      highFidelityTrainingValues = highFidelityValues(1:maxHighFidelitySize);
      highFidelityTestPoints = points((maxHighFidelitySize + 1):end, :);
      highFidelityTestValues = highFidelityValues((maxHighFidelitySize + 1):end);

      disp(sampleSizeArray);
      errorsArray(iteration, :) = getErrorsComparison(lowFidelityPoints, lowFidelityValues, ...
                                                      highFidelityTrainingPoints, highFidelityTrainingValues, ...
                                                      highFidelityTestPoints, highFidelityTestValues, ...
                                                      sampleSizeArray, dispErrors, options, dfOptions);
      disp(errorsArray(iteration, :));
    end

    % Proceed results
    [~, sortedIndexes] = sort(mean(errorsArray, 1));
    bestIndex = sortedIndexes(1);
    secondBestIndex = sortedIndexes(2);
    [pValue, h] = ranksum(errorsArray(:, bestIndex), errorsArray(:, secondBestIndex));
    disp(['Input dimension:', num2str(inputDimension)]);
    disp(pValue);
    disp(mean(errorsArray, 1));
    disp(3 * std(errorsArray, 1) / iterationNumber^0.5);
    csvwrite(['real_', realDataName, '_odim_', num2str(outputDimension), ...
              'cost_ratio', num2str(costRatio), '_budget_', num2str(budget), '_errors_corr_full.csv'], errorsArray);
  end
end  
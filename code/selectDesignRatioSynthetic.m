% Compare design ratios performance for different techniques

%% Setup of the run
dispErrors = 0;

iterationNumber = 25;
testSampleSize = 1000;

lowFidelityTheta = 2;
differenceTheta = 2;
noiseVariance = 1e-5;

% Set covariance function for training 
[options, dfOptions] = getTrainingOptions();
sampleCovarianceFunctionName = 'matern32';
shareArraySize = 9;
lowFidelityBudgetShareArray = reshape(linspace(0.1, 0.9, shareArraySize), shareArraySize, 1);

%% Run different synthetic setups
for setupParametersIndex = 1:11
  % get setup parameters
  setupParameters = getSetupParameters(setupParametersIndex);
  costRatio = setupParameters.costRatio;
  budget = setupParameters.budget;
  rho = setupParameters.rho; 
  inputDimension = setupParameters.inputDimension;
  lowFidelitySampleSize = budget;
  highFidelitySampleSize = budget;

  % Run procedure
  optimalDesignSizeRatio = (costRatio * rho^2)^(inputDimension / (inputDimension + 2)); % from theorem
  optimalLowFidelityBudgetShare = optimalDesignSizeRatio / (optimalDesignSizeRatio + costRatio);

  disp(['Optimal ratio:', num2str(optimalDesignSizeRatio)]);

  sampleSizeArray = zeros(shareArraySize + 2, 2);
  sampleSizeArray(1, :) =  [budget / costRatio, 0];
  sampleSizeArray(end, :) =  [0, budget];
  sampleSizeArray(2:end - 1, 1) = round(budget / costRatio * (1 - lowFidelityBudgetShareArray));
  sampleSizeArray(:, 2) = budget - costRatio * sampleSizeArray(:, 1);

  errorsArray = zeros(iterationNumber, size(sampleSizeArray, 1));
  
  csvFileName = ['synthetic_errors/synthetic_setup_index_', num2str(setupParametersIndex), '_', num2str(inputDimension), '_errors.csv'];
  if 1
    for iteration = 1:iterationNumber
        disp(['Iteration:', int2str(iteration)]);
        % generate sample
        [lowFidelityPoints, lowFidelityValues, ...
         highFidelityTrainingPoints, highFidelityTrainingValues, ...
         highFidelityTestPoints, highFidelityTestValues] = generateVariableFidelitySample(lowFidelityTheta, differenceTheta, ...
            rho, noiseVariance, lowFidelitySampleSize, highFidelitySampleSize, testSampleSize, inputDimension, ...
            sampleCovarianceFunctionName);           

        errorsArray(iteration, :) = getErrorsComparison(lowFidelityPoints, lowFidelityValues, ...
                                                        highFidelityTrainingPoints, highFidelityTrainingValues, ...
                                                        highFidelityTestPoints, highFidelityTestValues, ...
                                                        sampleSizeArray, dispErrors, options, dfOptions);
    end

    % Proceed results
    [~, sortedIndexes] = sort(mean(errorsArray, 1));
    bestIndex = sortedIndexes(1);
    secondBestIndex = sortedIndexes(2);
    [pValue, h] = ranksum(errorsArray(:, bestIndex), errorsArray(:, secondBestIndex));
    disp(['Input dimension:', num2str(inputDimension)]);
    disp(pValue);
    disp(mean(errorsArray, 1));
    disp(3 * std(errorsArray, 1) / iterationNumber^0.5);

    csvwrite(csvFileName , errorsArray);
  end
  errorsArray = dlmread(csvFileName); 
  
  figureName = ['synthetic_setup_index_', num2str(setupParametersIndex), '_id_', num2str(inputDimension), ...
                '_budget_', num2str(budget), '_errors.pdf'];
  plotBudgetShareVsError(errorsArray, optimalLowFidelityBudgetShare, lowFidelityBudgetShareArray, figureName);
  
%     techniqueList = {'High', 'EqualSize', 'EqualBudget', 'MinMinimax'};
%     saveDolanMore(errorsArray, ...
%                   ['x_', num2str(inputDimension)], techniqueList);
end
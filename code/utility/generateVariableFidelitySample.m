function [lowFidelityPoints, lowFidelityValues, ...
          highFidelityTrainingPoints, highFidelityTrainingValues, ...
          highFidelityTestPoints, highFidelityTestValues] = generateVariableFidelitySample(lowFidelityTheta, differenceTheta, ...
    rho, noiseVariance, lowFidelitySampleSize, highFidelitySampleSize, ...
    testSampleSize, inputDimension, covarianceFunctionName)
  % generate variable fidelity sample with specified parameters
  % generate all points and values
  if nargin < 9
    covarianceFunctionName = 'matern32';
  end
  
  allSampleSize = lowFidelitySampleSize + highFidelitySampleSize + testSampleSize;
  allPoints = rand(allSampleSize, inputDimension);
  allLowFidelityValues = generateGpSample(allPoints, lowFidelityTheta, noiseVariance, covarianceFunctionName);
  jointHighFidelityPoints = allPoints;
  differenceValues = generateGpSample(jointHighFidelityPoints, differenceTheta, noiseVariance, covarianceFunctionName);
  
  % split to low and high fidelity
  lowFidelityIndexes = 1:lowFidelitySampleSize;
  lowFidelityPoints = allPoints(lowFidelityIndexes , :);
  lowFidelityValues = allLowFidelityValues(lowFidelityIndexes, :);
  
  highFidelityTrainingIndexes = 1:highFidelitySampleSize;
  % highFidelityTrainingIndexes  = lowFidelitySampleSize + 1:lowFidelitySampleSize + highFidelitySampleSize;
  highFidelityTrainingPoints = allPoints(highFidelityTrainingIndexes, :);
  highFidelityTrainingValues = allLowFidelityValues(highFidelityTrainingIndexes, :);
  highFidelityTrainingValues = rho * highFidelityTrainingValues + ...
      differenceValues(highFidelityTrainingIndexes);
  
  % split high fidelity points to training and test
  % testPointsFirstIndex = lowFidelitySampleSize + highFidelitySampleSize + 1;
  % testPointsLastIndex = lowFidelitySampleSize + highFidelitySampleSize + testSampleSize;
  testPointsFirstIndex = max(lowFidelitySampleSize, highFidelitySampleSize) + 1;
  testPointsLastIndex = max(lowFidelitySampleSize, highFidelitySampleSize) + testSampleSize;
  highFidelityTestPoints = allPoints(testPointsFirstIndex:testPointsLastIndex, :);
  highFidelityTestValues = allLowFidelityValues(testPointsFirstIndex:testPointsLastIndex, :);
  highFidelityTestValues = rho * highFidelityTestValues + ...
      differenceValues(testPointsFirstIndex:testPointsLastIndex, :);
end


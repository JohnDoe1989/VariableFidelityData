function [points, lowFidelityValues, highFidelityValues, ...
          extraLowFidelityPoints, extraLowFidelityValues, ...
          inputDimension, costRatio] = loadRealData(realDataName, outputIndex)
  if nargin < 2
    outputIndex = 1;
  end
  
  folderName = '../data/';
  switch realDataName
      case 'press12'
        points = dlmread([folderName, 'press/points.csv']);
        lowFidelityValues = dlmread([folderName, 'press/moderate_values.csv']);
        highFidelityValues = dlmread([folderName, 'press/high_values.csv']);

        outputDimension = size(lowFidelityValues, 2);
        inputDimension = size(points, 2);
        extraLowFidelityPoints = zeros(0, inputDimension);
        extraLowFidelityValues = zeros(0, outputDimension);

        lowFidelityValues = lowFidelityValues(:, outputIndex); 
        highFidelityValues = highFidelityValues(:, outputIndex);
        extraLowFidelityValues = extraLowFidelityValues(:, outputIndex);

        costRatio = 5;
      case 'press13'
        points = dlmread([folderName, 'press/points.csv']);
        lowFidelityValues = dlmread([folderName, 'press/low_values.csv']);
        highFidelityValues = dlmread([folderName, 'press/high_values.csv']);
        
        outputDimension = size(lowFidelityValues, 2);
        inputDimension = size(points, 2);
        extraLowFidelityPoints = zeros(0, inputDimension);
        extraLowFidelityValues = zeros(0, outputDimension);

        lowFidelityValues = lowFidelityValues(:, outputIndex); 
        highFidelityValues = highFidelityValues(:, outputIndex);
        extraLowFidelityValues = extraLowFidelityValues(:, outputIndex);

        costRatio = 6;  
      case 'euler'
        points = dlmread([folderName, 'euler/points_common.csv']);
        lowFidelityValues = dlmread([folderName, 'euler/low_fidelity_values_common.csv']);
        highFidelityValues = dlmread([folderName, 'euler/high_fidelity_values_common.csv']);
        extraLowFidelityPoints = dlmread([folderName, 'euler/points_extra.csv']);
        extraLowFidelityValues = dlmread([folderName, 'euler/low_fidelity_values_extra.csv']);

        goodIndexes = sum(isnan(lowFidelityValues), 2) == 0;
        points = points(goodIndexes, :);
        lowFidelityValues = lowFidelityValues(goodIndexes, :);
        highFidelityValues = highFidelityValues(goodIndexes, :);

        goodIndexes = sum(isnan(extraLowFidelityValues), 2) == 0;
        extraLowFidelityPoints = extraLowFidelityPoints(goodIndexes, :);
        extraLowFidelityValues = extraLowFidelityValues(goodIndexes, :);

        lowFidelityValues = lowFidelityValues(:, outputIndex); 
        highFidelityValues = highFidelityValues(:, outputIndex);
        extraLowFidelityValues = extraLowFidelityValues(:, outputIndex);

        inputDimension = size(points, 2);
        costRatio = 5;
      case 'airfoil' 
        points = dlmread([folderName, 'airfoil/low_fidelity_points_dr_6.csv']);
        lowFidelityValues = dlmread([folderName, 'airfoil/low_fidelity_values.csv']);
        highFidelityValues = dlmread([folderName, 'airfoil/high_fidelity_values.csv']);

        sampleSize = size(highFidelityValues, 1);
        inputDimension = size(points, 2);
        
        extraLowFidelityPoints = points(sampleSize + 1:end, :);
        extraLowFidelityValues = lowFidelityValues(sampleSize + 1:end, :);

        points = points(1:sampleSize, :);
        lowFidelityValues = lowFidelityValues(1:sampleSize, :);

        lowFidelityValues = lowFidelityValues(:, outputIndex); 
        highFidelityValues = highFidelityValues(:, outputIndex);
        extraLowFidelityValues = extraLowFidelityValues(:, outputIndex);

        costRatio = 5;
      case 'disk'
        firstPoints = dlmread([folderName, 'disk/1_points.csv']);
        firstLowFidelityValues = dlmread([folderName, 'disk/1_low_fidelity_values.csv']);
        firstHighFidelityValues = dlmread([folderName, 'disk/1_high_fidelity_values.csv']);
        
        sampleSize = size(firstHighFidelityValues, 1);
        points = firstPoints(1:sampleSize, :);
        lowFidelityValues = firstLowFidelityValues(1:sampleSize, :);
        highFidelityValues = firstHighFidelityValues(1:sampleSize, :);
        
        extraLowFidelityPoints = firstPoints(1 + sampleSize:end, :);
        extraLowFidelityValues = firstLowFidelityValues(1 + sampleSize:end, :);

        secondPoints = dlmread([folderName, 'disk/1_points.csv']);
        secondLowFidelityValues = dlmread([folderName, 'disk/1_low_fidelity_values.csv']);
        secondHighFidelityValues = dlmread([folderName, 'disk/1_high_fidelity_values.csv']);
        
        sampleSize = size(secondHighFidelityValues, 1);
        points = [points; secondPoints(1:sampleSize, :)];
        lowFidelityValues = [lowFidelityValues; secondLowFidelityValues(1:sampleSize, :)];
        highFidelityValues = [highFidelityValues; secondHighFidelityValues(1:sampleSize, :)];
        
        extraLowFidelityPoints = [extraLowFidelityPoints; secondPoints(1 + sampleSize:end, :)];
        extraLowFidelityValues = [extraLowFidelityValues; secondLowFidelityValues(1 + sampleSize:end, :)];

        lowFidelityValues = lowFidelityValues(:, outputIndex); 
        highFidelityValues = highFidelityValues(:, outputIndex);
        extraLowFidelityValues = extraLowFidelityValues(:, outputIndex);
        
        inputDimension = size(points, 2);
        costRatio = 5;
    case 'machAngle'  
        points = dlmread([folderName, 'machAngle/points.csv']);
        lowFidelityValues = dlmread([folderName, 'machAngle/low_values_model.csv']);
        highFidelityValues = dlmread([folderName, 'machAngle/high_values.csv']);

        outputDimension = size(lowFidelityValues, 2);
        inputDimension = size(points, 2);
        extraLowFidelityPoints = zeros(0, inputDimension);
        extraLowFidelityValues = zeros(0, outputDimension);

        lowFidelityValues = lowFidelityValues(:, outputIndex); 
        highFidelityValues = highFidelityValues(:, outputIndex);
        extraLowFidelityValues = extraLowFidelityValues(:, outputIndex);

        costRatio = 5;
    case 'supernova'
        points = dlmread([folderName, 'supernova/points.csv']);
        lowFidelityValues = dlmread([folderName, 'supernova/low_values.csv']);
        highFidelityValues = dlmread([folderName, 'supernova/high_values.csv']);

        outputDimension = size(lowFidelityValues, 2);
        inputDimension = size(points, 2);
        extraLowFidelityPoints = zeros(0, inputDimension);
        extraLowFidelityValues = zeros(0, outputDimension);

        lowFidelityValues = lowFidelityValues(:, outputIndex); 
        highFidelityValues = highFidelityValues(:, outputIndex);
        extraLowFidelityValues = extraLowFidelityValues(:, outputIndex);

        costRatio = 5;  
     case 'svm'
        points = dlmread([folderName, 'svm/points.csv']);
        lowFidelityValues = dlmread([folderName, 'svm/low_values.csv']);
        highFidelityValues = dlmread([folderName, 'svm/high_values.csv']);

        outputDimension = size(lowFidelityValues, 2);
        inputDimension = size(points, 2);
        extraLowFidelityPoints = zeros(0, inputDimension);
        extraLowFidelityValues = zeros(0, outputDimension);

        lowFidelityValues = lowFidelityValues(:, outputIndex); 
        highFidelityValues = highFidelityValues(:, outputIndex);
        extraLowFidelityValues = extraLowFidelityValues(:, outputIndex);

        costRatio = 5;     
  end
  
  % normalize data
  meanValue = mean(highFidelityValues);
  stdValue = std(highFidelityValues);
  highFidelityValues = (highFidelityValues - meanValue) / stdValue;
  meanValue = mean(lowFidelityValues);
  stdValue = std(lowFidelityValues);
  lowFidelityValues = (lowFidelityValues - meanValue) / stdValue;
  extraLowFidelityValues = (extraLowFidelityValues - meanValue) / stdValue;
end
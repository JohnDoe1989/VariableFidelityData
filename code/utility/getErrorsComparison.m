function errorsIteration = getErrorsComparison(lowFidelityPoints, lowFidelityValues, ...
										      highFidelityTrainingPoints, highFidelityTrainingValues, ...
										      highFidelityTestPoints, highFidelityTestValues, ...
										      sampleSizeArray, dispErrors, options, dfOptions)
  % Compare errors for various ways to define ratio between sizes of variable fidelity samples
  % We assume that the first row in sampleSizeArray is for only high and the last is for only low fidelity data
  % Input: trainig and test samples of variable fidelities
  % Output: a row of rrms errors
  sampleSizeNumber = size(sampleSizeArray, 1);
  errorsIteration = zeros(sampleSizeNumber, 1);

  % train GP model
  highFidelityMaxSize = sampleSizeArray(1, 1);
  gpModel = gpTrain(highFidelityTrainingPoints(1:highFidelityMaxSize, :),  ...
                    highFidelityTrainingValues(1:highFidelityMaxSize, :), options);

  % get error fo GP model
  [testValuesSim, ~]  = gpSim(gpModel, highFidelityTestPoints);
  errorsIteration(1) = calcRRMS(highFidelityTestValues, testValuesSim);

  if dispErrors                                  
    disp(['0', ' ', ...
          int2str(highFidelityMaxSize), ' ', ...
          num2str(errorsIteration(1))]);
  end

  for index = 2:(sampleSizeNumber - 1)
    trainingHighFidelitySize = sampleSizeArray(index, 1);
    trainingLowFidelitySize = sampleSizeArray(index, 2);

    % construct VFGP model
    model = cokrigingTrain(lowFidelityPoints(1:trainingLowFidelitySize, :), ... 
                           lowFidelityValues(1:trainingLowFidelitySize, :), ...
                           highFidelityTrainingPoints(1:trainingHighFidelitySize, :),  ...
                           highFidelityTrainingValues(1:trainingHighFidelitySize, :), dfOptions);

    % get error for VFGP model
    [testValuesSim, ~]  = cokrigingSim(model, highFidelityTestPoints);
    errorsIteration(index) = calcRRMS(highFidelityTestValues, testValuesSim);
    if dispErrors
      disp([int2str(trainingLowFidelitySize), ' ', ...
            int2str(trainingHighFidelitySize), ' ', ...
            num2str(errorsIteration(index))]);
    end
  end

  % train low fidelity GP model
  lowFidelityMaxSize = sampleSizeArray(sampleSizeNumber, 2);
  lfgpModel = gpTrain(lowFidelityPoints(1:lowFidelityMaxSize, :),  ...
                    lowFidelityValues(1:lowFidelityMaxSize, :), options);

  % get error fo GP model
  [testValuesSim, ~]  = gpSim(lfgpModel, highFidelityTestPoints);
  errorsIteration(sampleSizeNumber) = calcRRMS(highFidelityTestValues, testValuesSim);

  if dispErrors                                  
    disp(['0', ' ', ...
          int2str(highFidelityMaxSize), ' ', ...
          num2str(errorsIteration(sampleSizeNumber))]);
  end
end
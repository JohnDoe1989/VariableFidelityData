function [rrsmError] = calcRRMS(trueValues, simValues)
    rrsmError = mean((simValues - trueValues).^2)^0.5 / ...
                std(trueValues);
end
function plotBudgetShareVsError(errorsArray, optimalLowFidelityBudgetShare, lowFidelityBudgetShareArray, figureName)
  figureHandle = figure();
  figureHandle.PaperPositionMode = 'auto';
  fig_pos = figureHandle.PaperPosition;
  figureHandle.PaperSize = [fig_pos(3) fig_pos(4)];

  iterationNumber = size(errorsArray, 1);
  ratioNumber = size(errorsArray, 2);
  hold on;
  errorbar(lowFidelityBudgetShareArray, mean(errorsArray(:, 2:(end - 1)), 1), ...
            2 * std(errorsArray(:, 2:(end - 1)), 0, 1) / iterationNumber^0.5, '-o', 'linewidth', 2);
  min_value = min(min(mean(errorsArray, 1) - 2 * std(errorsArray, 0, 1) / iterationNumber^0.5));
  max_value = max(max(mean(errorsArray, 1) + 2 * std(errorsArray, 0, 1) / iterationNumber^0.5));
  plot([optimalLowFidelityBudgetShare, optimalLowFidelityBudgetShare], ...
       [min_value, ...
        max_value], '--', 'linewidth', 2);
  errorbar([0, 1], mean(errorsArray(:, [1, ratioNumber]), 1), ...
            2 * std(errorsArray(:, [1, ratioNumber]), 0, 1) / iterationNumber^0.5, 'o', 'linewidth', 2);
          
  xlabel('Low fidelity evaluations budget proportion', 'fontsize', 18);
  ylabel('RRMS error', 'fontsize', 18);
  axis tight;
  xlim([-0.05, 1.05]);
  legendHandle = legend('Errors', 'Minimax optimal proportion', 'Single fidelity errors', 'location', 'northwest');
  set(legendHandle, 'FontSize', 18);
  grid on;
  print(figureHandle, figureName, '-dpdf')
  pause(2);
  close all;
end
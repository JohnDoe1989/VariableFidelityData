function [options, dfOptions] = getTrainingOptions()
  % set covariance function for training 
  options = {};
  options.debugInfo = 0;
  covarianceOptions.power = 1;
  % options.covarianceFunction = {@covarianceExpArd, covarianceOptions};
  options.covarianceFunction = {@covarianceMatern32, covarianceOptions};
  dfOptions = {};
  dfOptions.debugInfo = 0;
  dfOptions.fine = options;
  dfOptions.coarse = options;   
end
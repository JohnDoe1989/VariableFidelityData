function setupParameters = getSetupParameters(setupNumber)
   % get different setup for synthetic data problems
	switch setupNumber   
    case 1  
      budget = 300;
      costRatio = 5;
      correlation = 0.7;
      rho = 1 / (1 / correlation^2 - 1)^0.5;
      inputDimension = 3;
    case 2  
      budget = 300;
      costRatio = 5;
      correlation = 0.8;
      rho = 1 / (1 / correlation^2 - 1)^0.5;
      inputDimension = 3;  
    case 3  
      budget = 300;
      costRatio = 5;
      correlation = 0.9;
      rho = 1 / (1 / correlation^2 - 1)^0.5;
      inputDimension = 3;  
    case 4  
      budget = 300;
      costRatio = 5;
      correlation = 0.95;
      rho = 1 / (1 / correlation^2 - 1)^0.5;
      inputDimension = 3;    
    case 5  
      budget = 300;
      costRatio = 10;
      correlation = 0.7;
      rho = 1 / (1 / correlation^2 - 1)^0.5;
      inputDimension = 3;
    case 6  
      budget = 300;
      costRatio = 10;
      correlation = 0.8;
      rho = 1 / (1 / correlation^2 - 1)^0.5;
      inputDimension = 3;  
    case 7  
      budget = 300;
      costRatio = 10;
      correlation = 0.9;
      rho = 1 / (1 / correlation^2 - 1)^0.5;
      inputDimension = 3;
    case 8  
      budget = 300;
      costRatio = 10;
      correlation = 0.9;
      rho = 1 / (1 / correlation^2 - 1)^0.5;
      inputDimension = 3;
    case 9  
      budget = 300;
      costRatio = 10;
      correlation = 0.95;
      rho = 1 / (1 / correlation^2 - 1)^0.5;
      inputDimension = 3;
    case 10 
      budget = 300;
      costRatio = 3;
      correlation = 0.8;
      rho = 1 / (1 / correlation^2 - 1)^0.5;
      inputDimension = 3;  
    case 11
      budget = 300;
      costRatio = 3;
      correlation = 0.9;
      rho = 1 / (1 / correlation^2 - 1)^0.5;
      inputDimension = 3;   
  end
  
  budget = 300; % TODO remove
  setupParameters.inputDimension = inputDimension;
  setupParameters.costRatio = costRatio;
  setupParameters.rho = rho;
  setupParameters.budget = budget;
end
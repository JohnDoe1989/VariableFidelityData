function [ values ] = generateGpSample(points, theta, noiseVariance, covarianceFunctionName)
  % generate sample with desired parameters
  % set parameters 
  if nargin < 4
    covarianceFunctionName = 'squaredExponential';
  end

  sampleSize = size(points, 1);
  inputDimension = size(points, 2);
  
  switch covarianceFunctionName 
    case 'exponential' 
      options.power = 1; 
    case 'squaredExponential'
      options.power = 2;
    otherwise  
      options.power = 1; 
  end
  options.isDiagonal = 0;
  hyperParameters.covarianceVariance = log(theta) * ones(inputDimension, 1);
  
  % generate covariance matrices
  switch covarianceFunctionName
  case 'squaredExponential'
      covarianceMatrix = covarianceExpArd(options, hyperParameters, points);
    case 'exponential'
      covarianceMatrix = covarianceExpArd(options, hyperParameters, points);
    case 'matern32'
      covarianceMatrix = covarianceMatern32(options, hyperParameters, points);
  end
  
  covarianceMatrix = covarianceMatrix + noiseVariance * eye(sampleSize);
  choleskyCovarianceMatrix = chol(covarianceMatrix);
  
  % generate sample from desired multivariate normal distribution
  initialValues = randn(sampleSize, 1);
  values = choleskyCovarianceMatrix' * initialValues;
end

